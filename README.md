regsub
======

## Features

### Complements POSIX `regex`

* Provides sed-like '`s`' substitution
* Global matching

### GAWK API

Implements the following `gawk` functions in C:

* [`gensub()`](https://www.gnu.org/software/gawk/manual/html_node/String-Functions.html#index-gensub_0028_0029-function-_0028gawk_0029-1)
* [`gsub()`](https://www.gnu.org/software/gawk/manual/html_node/String-Functions.html#index-gsub_0028_0029-function-1)

## Build & install

### Build

This is a fairly ordinary `cmake` project, build as follows:

``` bash
mkdir build; cd build
cmake ../
make -j1
```

To build without installable man-pages (and a small test-program), add
`-DREGSUB_BUILD_TESTS=no` to `cmake` above.

#### Quirks

If `cmake` complains about a missing package library or tool, follow the
instructions and install them. This should be pretty self-explanatory for
most packages except one: `doxy2man`

##### doxy2man

The tool `doxy2man` is a clever little program that generates beautiful
library indexed and grouped man-pages. It takes away the incredible pain of
(especially) creating library manages. But more so, it does it on a
function-level and incredibly beautifully. It's however probably not in your
OS list of installable packages, and left somewhat unmaintained since 2016
which means build/install instructions are somewhat stale. Here's how to
build-install on Debian 12 which should work for Ubuntu 20.04 and Debian 11
as well:

The actual quirk is that it's based on `Qt` but supported only up to `Qt5`
which is not default on Debian distros anymore and mixing qt-versions (or
install at all on a Gnome-based distro) is PITA to say the least. Make sure
you have (only?) the following installed:

``` bash
apt install libqt5xmlpatterns5-dev \
    qtxmlpatterns5-dev-tools \
    qtxmlpatterns5 \
    qtbase5-dev qtbase5-dev-tools
```

Then (hopefully) the following will work:

``` bash
cd somewhere
git clone https://github.com/gsauthof/doxy2man
cd doxy2man
qmake
make && sudo cp doxy2man /usr/local/bin/
```

### Install

Assuming Debian package (default)

``` bash
cd build ## If not standing there
make package
sudo dpkg -i *.deb
```

If building an installable package for another platform, use `ccmake` .` to
change `SPECIFIED_CPACK` to any other of the supported ones.
