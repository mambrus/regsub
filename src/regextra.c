/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "regextra.h"
#include "regsub.h"
#include <stdio.h>
#include <regex.h>
#include <assert.h>
#include <liblog/assure.h>
#include <liblog/log.h>
#include "common.h"

#define DEF_CFLAGS (REG_EXTENDED | REG_NEWLINE)
#define DEF_EFLAGS (0)

regmatch_t regfind(const char *re, const char *str, size_t nth)
{
    regex_t preg;
    size_t max_match = (nth == 0) ? 2 : nth + 1; /* At least two */
    regmatch_t match[max_match];
    int rc;

    for (size_t i = 0; i < max_match; i++) {
        match[i].rm_so = -2;
        match[i].rm_eo = -2;
    }

    rc = regcomp(&preg, re, DEF_CFLAGS);
    if (rc != 0) {
        char buf[100];
        regerror(rc, &preg, buf, 100);
        LOGE("%s: Bad regex detected: %s (%s)\n", re, __func__, buf);
        goto nomatch;
    }

    if (nth > 0) {
        rc = gregexec(&preg, str, max_match, match, DEF_EFLAGS);
    } else {
        rc = regexec(&preg, str, max_match, match, DEF_EFLAGS);
    }
    regfree(&preg);

    if (rc == REG_NOMATCH) {
        return (regmatch_t){ -1, -1 };
    }
    size_t n = regmatches(match);
    if (n) {
        if (nth > 0)
            if (n < max_match)
                return match[nth + 1];
            else
                goto nomatch;
        else
            return match[0];
    }
nomatch:
    return (regmatch_t){ -1, -1 };
}

bool regexist(const char *re, const char *str, size_t nth)
{
    regmatch_t match = regfind(re, str, nth);

    if (regmatches(&match) >= 1)
        return true;

    return false;
}

size_t regmatches(regmatch_t pmatch[])
{
    int i;

    if (!pmatch || (pmatch[0].rm_eo < 0) || (pmatch[0].rm_so < 0))
        return 0;

    if ((pmatch[1].rm_so >= 0) || ((pmatch[0].rm_eo >= pmatch[0].rm_so >= 0))) {
        for (i = 1; pmatch[i].rm_so >= 0 && pmatch[i].rm_eo >= 0; i++)
            ;
        return pmatch[i - 1].rm_so >= 0 ? i : 0;
    }
    for (i = 0; pmatch[i].rm_so >= 0 && pmatch[i].rm_eo >= 0; i++)
        ;
    return pmatch[i].rm_so >= 0 ? i : 0;
}
