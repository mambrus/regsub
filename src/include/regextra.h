/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/** \file regextra.h
 *
 * \brief Extend POSIX regex library with re-indexing missing conditionals
 *
 * Extend POSIX regex. Link with \em -lregsub
 *
 * \sa regex.h
 * \sa regsub.h
 *
 * \copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>
 *
 */
#ifndef regextra_h
#define regextra_h

#include <regex.h>
#include <stdbool.h>
#include <stdlib.h>

/** \brief Find regular expression in string
 *
 * Search the target string \em re for matches of the regular expression \em
 * str.
 *
 * \b NOTE: If several matches exist in \em str, only one match is returned.
 * Which one is determined by \em nth. If several matches are found, but not
 * within \em nth the result will be no match.
 *
 * \param re                Regular expression string
 * \param str               String to each-for
 * \param nth               If several matches, return result for the \em
 *                          nth. Pass value \em 0 to always consider only
 *                          the first match.
 *
 * \retval regmatch_t:      Index of start and end of found \em re
 */
regmatch_t regfind(const char *re, const char *str, size_t nth);

/** \brief Determine if regular expression exists in string
 *
 * Identical to \em regfind but satisfy with a simple true/false.
 * 
 * \sa regfind
 *
 * \b NOTE: If several matches exist in \em str, only one match is
 * considered. Which one is determined by \em nth. If several matches are
 * found, but not within \em nth the result will be no match.
 *
 * \param re                Regular expression string
 * \param str               String to search-for
 * \param nth               If several matches, return result for the \em
 *                          nth. Pass value \em 0 to always consider only
 *                          the first match.
 *
 * \retval regmatch_t:      Index of start and end of found \em re
 */
bool regexist(const char *re, const char *str, size_t nth);

/** \brief Return number of matches
 *
 * \sa regexec
 *
 * \param pmatch            Array of matches returned by regexec
 *
 * \retval int:             Number of matches
 */
size_t regmatches(regmatch_t pmatch[]);

#endif /* regextra_h */
