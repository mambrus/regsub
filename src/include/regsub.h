/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef regsub_h
#define regsub_h

#include <regex.h>
#include <stdbool.h>

/** \file regsub.h
 *
 * \brief Extend POSIX regex library
 *
 * Sed-like high-level functions and global matching. Link with \em -lregsub
 *
 * \sa regex.h
 * \sa gensub.h
 *
 * \copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>
 *
 */

/** \brief Global match
 *
 * Extend POSIX-regex by finding all matches recursively
 *
 * \sa regexec(3)
 * \sa pcre_exec(3)
 *
 * \param preg      Pre-compiled  regex_t
 * \param string    String from which to searches matches
 * \param nmatch    Size of pmatch array. Also the maximum number of matches
 *                  -1 as last element is a sentinel.
 * \param pmatch    Array of matches. Same as for \em regex(), except that it
 *                  contains all matches. Parameter mirrors regex's pmatch
 *                  with the following extended meaning:
 *                  1) re_so in the first position is identical but re_so is
 *                     not.
 *                  2) re_eo is the last string-index of the global regex
 *                     match in total. I.e. the elements can not be assumed
 *                     to map continuously and can/will contain match-gaps.
 * \param eflags    Parameter mirrors regex's \em eflag with the following
 *                  exception:
 *                  \b REG_NOBOL is appended automatically on all but the
 *                    first recursion to prevent matching beginning-of-line
 *                    anchor (^) more than once.
 * \retval          int: 0 on success or \b REG_NOTFOUND on error
 *
 * \author Michael Ambrus <michael@ambrus.se>
 */

int gregexec(const regex_t *preg, const char *string, size_t nmatch,
             regmatch_t pmatch[], int eflags);

/** \brief High level sed-like (-E) s-command function
 *
 * Compile arbitrary regex \em (r) and execute substitutions in \em (s),
 * globally \em (g) if requested. Operates from target-string \em (t) unless
 * \em NULL, otherwise \em stdin. Returns the substituted string which needs
 * to be freed whence done, unless \em (t) was NULL in which case output is
 * \em stdout.
 *
 * \sa sed(1)
 *
 * \param r         String containing extended regular expression.
 * \param s         Substitution string including optional reference tags
 *                  \em \\n where \em n is a number from \em 0-9.
 * \param g         If \em true, replace globally.
 * \param t         If set, construct result from this parameter. Argument is
 *                  freed if a successful new string is generated and
 *                  returned. In which case it's reference is additionaly
 *                  set to NULL to prevent accidental double free.
 *                  If not set \em (NULL), input will be taken from \em
 *                  stdin and output subsequently sent to \em stdout.
 *                  Requirements:
 *                  1) Must be a reference to a pointer-variable
 *                  2) which in turn must not reference a constant string.
 *
 * \retval          char*: A new string with replacement actuated, \em NULL on
 *                  failure/stdio or reference to \em t on no-match.
 *                  If \em stdio operation (see parameter t), \em NULL is
 *                  subsequently also returned..
 *
 * \author Michael Ambrus <michael@ambrus.se>
 */
char *regsub(const char *r, const char *s, bool g, const char *t);

#endif /* regsub_h */
