/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/** \file gensub.h
 *
 * \brief Extend POSIX regex library with gawk functions
 *
 * Gawk regex-functions in C to extend POSIX regex. Link with \em -lregsub
 *
 * \sa regex.h
 * \sa regsub.h
 *
 * \copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>
 *
 */
#ifndef gensub_h
#define gensub_h

#include <regex.h>

/** \brief High-level generic regular expression substitute
 *
 * Search the target string \em t for matches of the regular expression \em
 * r. If \em h is character \b g or \b G, then replace all matches of \em r
 * with \em s. Otherwise, \em h is a number indicating which match of \em r
 * to replace.  If \em t is NULL, use \b stdin instead. Within the
 * replacement-text references in the form \\\\n may be used to refer back
 * to matches in the target string \em t, where \em n is a digit from 1 to 9, to
 * indicate just the text that matched the n'th parenthesized subexpression.
 * The sequence \\\\0 represents  the entire matched text, as does the
 * character &. Unlike sub() and gsub(), the modified string is returned as
 * the  result of  the function, and the original target string is not
 * changed.
 *
 * \b NOTE: Function operates like strdup(). I.e. it returns a new string which
 * needs to be freed whence used.
 *
 * \param r         Regular expression string
 * \param s         Formatted substitution string
 * \param h         Which sub-match to substitute (short int), or global if
 *                  'G'/'g' (char)
 * \param t         From target string, or NULL if stdio input/output
 *                  operation
 * \retval char*:   Generated string or NULL upon error or stdio operation
 */
char *gensub(const char *r, const char *s, char h, const char *t);

/** \brief High-level regular expression substitute
 *
 * For  each  substring  matching  the regular expression \em r in the
 * string \em t, substitute the string \em s, and return the number of
 * substitutions.  If \em t is \b NULL, use stdin/stdout.  An & in the
 * replacement text is replaced with the text that was actually matched.
 * Use \\& to get a literal &.   (This  must be  typed as "\\\\&"; see GAWK:
 * Effective AWK Programming for a fuller discussion of the rules for
 * ampersands and backslashes  in  the  replacement text of sub(), gsub(),
 * and gensub().)
 *
 * \b NOTE: Function frees the target-string "t". I.e. usage MUST comply to
 * the following:
 *
 * - If "t" is a pointer it must not be freed again (double free).
 * - "t" must be a variable, i.e. it can't be a constant or literal string.
 *
 * \param r         Regular expression string
 * \param s         Formatted substitution string
 * \param t         From target string, or NULL if stdio input/output
 * \retval int:     Number of successful replacements
 */
int gsub(const char *r, const char *s, char *t[]);

#endif /* gensub_h */
