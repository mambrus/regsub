/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef regsub_main_h
#define regsub_main_h

#include <stdbool.h>
#include <liblog/log.h>
#include <stdio.h>

/* Options struct used while command-line options are parsed. */
struct opts {
    log_level *loglevel; /* Verbosity level */
    int daemon;          /* If to become a daemon or not */
    bool global;         /* Replace globally (sed 's' g-flag) */
    FILE *in;            /* Read from */
    FILE *out;           /* Write to */

    struct req_opt *req_opts; /* Deep copy of the req_opts list. Used to
                                   extend logic with presence validation. */
};

void regsub_exit(int status);
int main(int argc, char **argv);

#endif /* regsub_main_h */
