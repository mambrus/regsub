/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef regsub_common_h
#define regsub_common_h
#include <regex.h>
#include <errno.h>
#include "config.h"

#ifndef REGSUB_BUILD_TYPE
#error Build-type for project regsub is undefined
#endif

#if !defined(REGSUB_LOG_REGARDLESS_BUILDTYPE) && \
    (REGSUB_BUILD_TYPE != BUILD_TYPE_DEBUG)
/* Nuke all logs regardless log-level that are not error */
#undef LOGV
#undef LOGD
#undef LOGI
#undef LOGW

#define LOGV(...)    ((void)(0))
#define LOGD(...)    ((void)(0))
#define LOGI(...)    ((void)(0))
#define LOGW(...)    ((void)(0))
#define RM_DUMP(...) ((void)(0))
#else
#define RM_DUMP(RE) rm_dump(#RE, RE)
#endif

#define DEF_CFLAGS (REG_EXTENDED | REG_NEWLINE)
#define DEF_EFLAGS (0)
#define UNUSED(x)  (void)(x)
#define NOP        ((void)0)
#define ASZ(A)     (sizeof(A) / sizeof(A[0]))

/* Compensate possible lack of built-in __FILE_NAME__:
 * https://gcc.gnu.org/onlinedocs/cpp/Common-Predefined-Macros.html
 */
#ifndef __FILE_NAME__
#define __FILE_NAME__ \
    (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif
#define MAX_MATCH 'G' /*Last possible reference is 'G'-1 = 70 */
#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))
#define MATCHES(RM, MAX)                                            \
    ({                                                              \
        size_t m;                                                   \
        for (m = 0; (m < MAX) && (RM[m].rm_so != RM[m].rm_eo); m++) \
            ;                                                       \
        m;                                                          \
    })

/* Size for any kind of temp-buffer */
#define BUF_SZ 1000
/* One less to fit trailing \0 in case temp-buffer is a string */
#define CHARS_MAX (BUF_SZ - 1)

/*
 *  Executes regexec/regcomp or compatible functions, but log errors on
 *  failure (IIF debug builds). Compatibility is determined by the return
 *  type and meaning.
 */
#if REGSUB_BUILD_TYPE == BUILD_TYPE_DEBUG
#define REG(FN, PREG, ...)                                                 \
    ({                                                                     \
        int rc;                                                            \
        rc = FN(PREG, __VA_ARGS__);                                        \
        if (rc != 0) {                                                     \
            char buf[BUF_SZ] = { 0 };                                      \
            regerror(rc, PREG, buf, CHARS_MAX);                            \
            LOGV("%s - %s error l#%d: %s\n", __FILE_NAME__, #FN, __LINE__, \
                 buf);                                                     \
            errno = EBADE;                                                 \
        }                                                                  \
        rc;                                                                \
    })
#else
#define REG(FN, PREG, ...) (FN(PREG, __VA_ARGS__))
#endif

/*
 * Sub-group match. Each index points to another index in a regmatch_t
 * array.
 */
typedef struct {
    char begin;     /* Begins at index */
    char end;       /* Ends at index */
    regmatch_t hdr; /* Sub-match header (i.e. zero'th array
                                   index in a regmatch_t[]) */
} smatch_t;

/*
 * Global matching
 */
struct gmatch {
    regex_t regex;      /* The compiled regex_t */
    regmatch_t *pmatch; /* Main matches */
    regmatch_t *rmatch; /* Reference matches (forward references) */
    smatch_t *smatch;   /* Groups of matches - what POSIX regexp
                                   consider one sub-match */
    size_t nmatch;      /* Sizes of regmatch_t (see POSIX regexec()) */
    int cflags;         /* See corresponding argument in POSIX regcomp()
                                 */
    int eflags;         /* See corresponding argument in POSIX regexp()
                                 */
    int depth;          /* Current depth while operating recursively */
    int pfound;         /* Matches found in main-match */
    int rfound;         /* Matches found in reference-match */
    int sfound;         /* Number of sub-matches found */
    int rglobal;        /* Substitute globally */
    int rindex;         /* Substitute which specific match (if not
                                   global) */
};

#define SUBST_SIGNATURE \
    const char *s, const char *t, regmatch_t tm[], regmatch_t sm[]
#define UNUSED_ALL_SUB \
    UNUSED(s);         \
    UNUSED(t);         \
    UNUSED(tm);        \
    UNUSED(sm)
typedef char *(*getstr_t)(SUBST_SIGNATURE, int);
char *sub(SUBST_SIGNATURE, getstr_t getstr);
char *lam_backref_str(SUBST_SIGNATURE, int k);
char *sub_backref_str(SUBST_SIGNATURE);
char *sub_replace_str(SUBST_SIGNATURE);

void rm_dump(const char *name, regmatch_t rm[]);
regex_t *get_refreg(void);

/*
 * Back-end functions that can re-used between c-files
 */
int _rregexec(const char *string, struct gmatch *gmatch);
int _gregexec(const regex_t *preg, const char *string, size_t nmatch,
              regmatch_t pmatch[], int eflags, struct gmatch *cgmatch);

#endif // regsub_common_h
