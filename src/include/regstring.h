/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/** \file regstring.h
 *
 * \brief String helper-functions
 *
 * Concatenate, append, prepend assign and free
 *
 * \sa regstring.h
 *
 * \copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>
 *
 */
#ifndef regstring_h
#define regstring_h
#include <stddef.h>

/** \brief Safely assign a string-variable (macro)
 *
 * Wrap \em strassign to make it easier to use. Example:
 *
 * STRASGN( string ) = "Hello world";
 *
 * \sa strassign
 *
 * \param V                 Name of the string variable
 */
#define STRASGN(V) *strassign(&V)

/** \brief Safely assign a string-variable
 *
 * Assign a string-pointer the address of another.
 * Free string-variable if not NULL before it gets assigned
 * again to prevent memory leakage.
 *
 * \param string            Reference-pointer to string variable
 *
 * \retval char**:          Address to reference
 */
char **strassign(char **string);

/** \brief Safely free string-variable
 *
 * Conditionally free string if not NULL and set it's reference variable to
 * NULL after free.
 *
 * \param string            Reference-pointer to string variable
 */
void strfree(char **string);

/** \brief Append string of maximum size
 *
 *  Return a new string that is B appended to A. Free A before assigning
 *  it's reference the new string.
 *
 *  NOTE: A is overwritten and replaced.
 *
 * \param A                 Reference-pointer to string to append to
 * \param B                 String to append
 * \param n                 Maximum length of the appender
 *
 * \retval char*:           Address to the new string
 */
char *strnappnd(char **A, const char *B, size_t n);

/** \brief Append string
 *
 *  Return a new string that is A prepended to B. Free A before assigning
 *  it's reference the new string.
 *
 *  NOTE: A is overwritten and replaced.
 *
 * \param A                 Reference-pointer to string to append to
 * \param B                 String to append
 *
 * \retval char*:           Address to the new string
 *
 *  Return a new string that is B appended to A. Free A before returning.
 */
char *strappnd(char **A, const char *B);

/** \brief Prepend string of maximum size
 *
 *  Return a new string that is A prepended to B. Free B before assigning
 *  it's reference the new string.
 *
 *  NOTE: B is overwritten and replaced.
 *
 * \param A                 String to prepend
 * \param B                 Reference-pointer to string to prepend to
 * \param n                 Maximum length of the prepender
 *
 * \retval char*:           Address to the new string
 */
char *strnprepend(const char *A, char **B, size_t n);

/** \brief Prepend string
 *
 *  Return a new string that is A prepended to B. Free B before assigning
 *  it's reference the new string.
 *
 *  NOTE: B is overwritten and replaced.
 *
 *
 * \param A                 String to prepend
 * \param B                 Reference-pointer to string to prepend to
 *
 * \retval char*:           Address to the new string
 */
char *strprepend(const char *A, char **B);

#endif /* regextra_h */
