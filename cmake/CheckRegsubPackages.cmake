
CHECK_LIBRARY_EXISTS(liblog liblog_set_verbosity "/usr/local/lib/" HAVE_LIB_LIBLOG)

#
# Dependant libraries possibly not installed
#
set(LIB_LIBLOG_PATH         "${PROJECT_SOURCE_DIR}/lib/liblog"
    CACHE STRING "Source-path to library 'liblog'")
#
# Offer to build these libraries even if not needed
#
if (HAVE_LIB_LIBLOG)
    set (EXTRA_LIBS ${EXTRA_LIBS} liblog)
    option(REGSUB_BUILD_LIBLOG "Library liblog from source" OFF)
else (HAVE_LIB_LIBLOG)
    if(EXISTS "${LIB_LIBLOG_PATH}")
        message ("** WARNING: Liblog not installed. Defaults to build from source")
        find_package(liblog 0.3.9 REQUIRED)
        option(REGSUB_BUILD_LIBLOG "Library liblog from source" ON)
    else()
        message (SEND_ERROR
        "Dependent library liblog isn't installed"
        "Please build from: "
        "https://github.com/mambrus/liblog or "
        "Or install: "
        "sudo apt install liblog ")
    endif()
endif (HAVE_LIB_LIBLOG)

if (REGSUB_BUILD_LIBLOG)
    option(LIBLOG_ENABLE_SYSLOG     "Logs to syslog" ON)
    option(LIBLOG_SYSLOG_LOG_PERROR "Log to also stderr if syslog" OFF)
    set(LIBLOG_DEFAULT_LOG_LEVEL
        ${LIBLOG_DEFAULT_LOG_LEVEL}INFO
        CACHE STRING
        "Set explicit default log-level: [0-6] | [VERBOSE|DEBUG|INFO|WARNING|ERROR|CRITICAL|SILENT|SYS]")
    add_subdirectory(${LIB_LIBLOG_PATH} "${PROJECT_BINARY_DIR}/build/liblog")
    set(REGSUB_LIBS ${REGSUB_LIBS} liblog_static)
    list(APPEND DFLT_REGSUB_BUILD_OPTIONS ${LIBLOG_BUILD_OPTIONS})
else ()
    list(APPEND DFLT_REGSUB_BUILD_OPTIONS "-Wl,--undefined=__liblog_init")
endif()

set (LOG_LEVEL
    ${LOG_LEVEL} LOG_LEVEL_INFO
    CACHE STRING
    "Default LOG_LEVEL (over-ride with env-var)")

set_property(CACHE LOG_LEVEL
    PROPERTY STRINGS
    "LOG_LEVEL_VERBOSE"
    "LOG_LEVEL_DEBUG"
    "LOG_LEVEL_INFO"
    "LOG_LEVEL_WARNING"
    "LOG_LEVEL_ERROR"
    "LOG_LEVEL_CRITICAL"
    "LOG_LEVEL_SILENT")

message(STATUS "DFLT_REGSUB_BUILD_OPTIONS: ${DFLT_REGSUB_BUILD_OPTIONS}")

set(REGSUB_BUILD_OPTIONS
    ${DFLT_REGSUB_BUILD_OPTIONS}
    CACHE STRING
    "Extra build options. Defaults iherited from sub-projects")
mark_as_advanced(REGSUB_BUILD_OPTIONS)

string(REPLACE ";" " " _REGSUB_BUILD_OPTIONS "${REGSUB_BUILD_OPTIONS}")
add_compile_options(${_REGSUB_BUILD_OPTIONS})
add_compile_options(${CMAKE_EXTRA_C_FLAGS})

# Notes to self:
# ==========================================================
#   add_link_options(${REGSUB_BUILD_OPTIONS})    // CMake +3.15.5 needed

