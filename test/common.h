/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef regsub_test_common_h
#define regsub_test_common_h

#ifndef REGSUB_BUILD_TYPE
#error Build-type for project regsub is undefined
#endif

#if REGSUB_BUILD_TYPE != BUILD_TYPE_DEBUG
/* Nuke all logs that are not info or error regardless log-level */
#undef LOGV
#undef LOGD
#undef LOGW

#define LOGV(...) ((void)(0))
#define LOGD(...) ((void)(0))
#define LOGW(...) ((void)(0))
#endif

unsigned long long memphys();
unsigned long long memavail();

#endif // regsub_test_common_h
