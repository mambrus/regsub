/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "regsub.h"
#include <asm-generic/errno-base.h>
#include <stdio.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <stdint.h>
#include <errno.h>
#include <liblog/assure.h>
#include <assert.h>
#include "common.h"

/*
 * Back-end of rregexec(). Same arguments except struct gmatch which is used
 * recursively to avoid having to re-allocate/re-compile.
 *
 * gmatch: Is a return parameter.
 */
int _rregexec(const char *string, struct gmatch *gmatch)
{
    int rc, eo = 0;

    gmatch->depth++;
    eo = gmatch->pmatch[gmatch->rfound - 1].rm_eo;
    assert((eo >= 0) && ((unsigned)eo <= strlen(string)));

    rc = regexec(&gmatch->regex, &string[eo], gmatch->nmatch, gmatch->rmatch,
                 gmatch->eflags);

    if (rc != REG_NOMATCH) {
        if (gmatch->depth == 0) {
            gmatch->pmatch[0].rm_so = gmatch->rmatch[0].rm_so;
            gmatch->eflags |= REG_NOTBOL;
        }
        /* Extend latest's found eo in the first element of the destination */
        int neo = gmatch->rmatch[0].rm_eo + eo;
        gmatch->pmatch[0].rm_eo = neo;
        size_t i, m, n = gmatch->rfound, o, k = 0;

        m = MATCHES(gmatch->rmatch, gmatch->nmatch);

        /* Detect repetitive patterns where POSIX regex() clusters inner
         * matches into one and one only. I.e. the in-betweens are not in
         * the regmatch_t[], only the last one is. As we don't know which
         * ones these would be and as we don't have access to the (r),
         * (&r[(gmatch->rmatch[m-1]).rm_eo-1] == '+') would be ideal,
         * detect that there are only one match (the cluster) besides the
         * header and extend rm_so to header rm_so */
        if ((m == 2) && (gmatch->rmatch[1].rm_so > gmatch->rmatch[0].rm_so)) {
            LOGW("Repetitive pattern detected:"
                 " (1).re_so > (0).re_so: %d >= %d: \"%s\"\n",
                 gmatch->rmatch[1].rm_so, gmatch->rmatch[0].rm_so, string);
            gmatch->rmatch[1].rm_so = gmatch->rmatch[0].rm_so;
        }

        /* If can store sub-match groups and last regexec is a new group,
         * record it */
        if (gmatch->smatch && (m > 1)) {
            int si = gmatch->sfound;
            gmatch->smatch[si].begin = gmatch->rfound;
            gmatch->smatch[si].end = gmatch->rfound + m - 2;
            gmatch->smatch[si].hdr.rm_so = gmatch->rmatch[0].rm_so + eo;
            gmatch->smatch[si].hdr.rm_eo = gmatch->rmatch[0].rm_eo + eo;
            si++;
            gmatch->smatch[si] = (smatch_t){ -1, -1, { -1, -1 } };
            gmatch->sfound = si;
        }
        /* if m==1 (i.e. no sub-match but main regex matches), treat
         * specially - reoccurring */
        if (m == 1) {
            m++;
            k = 1;
        }
        for (o = n, i = (1 - k); i < (m - k); o++, i++) {
            gmatch->pmatch[o].rm_so = gmatch->rmatch[i].rm_so + eo;
            gmatch->pmatch[o].rm_eo = gmatch->rmatch[i].rm_eo + eo;
        }
        gmatch->pmatch[o] = (regmatch_t){ -1, -1 };
        gmatch->rfound = MATCHES(gmatch->pmatch, gmatch->nmatch);
        /* if m==1 (i.e. no sub-match but main regex matches), treat
         * specially - first iteration only */
        (gmatch->rfound == 1) ? gmatch->rfound++ : NOP;
        _rregexec(string, gmatch);
    }

    /* Note next reader: Can't summarise cumulative values from gmatch here as
     * function is recursive. I.e. it has to be done by caller (or use extra
     * state variables used which we don't want) */
    return rc;
}

/*
 * Backend of gregexec()
 */
int _gregexec(const regex_t *preg, const char *string, size_t nmatch,
              regmatch_t pmatch[], int eflags, struct gmatch *cgmatch)
{
    int rc = REG_NOMATCH;
    regmatch_t *rmatch;
    if (!(preg && string && pmatch)) {
        errno = EINVAL;
        return REG_NOMATCH;
    }
    pmatch[0] = (regmatch_t){ 0, 0 };
    struct gmatch gmatch;

    rmatch = calloc(nmatch, sizeof(regmatch_t));
    if (!rmatch) {
        errno = ENOMEM;
        return REG_NOMATCH;
    }

    if (cgmatch)
        gmatch = *cgmatch;

    gmatch.regex = *preg;
    gmatch.nmatch = nmatch;
    gmatch.rmatch = rmatch;
    gmatch.pmatch = pmatch;
    gmatch.eflags = eflags;
    gmatch.pfound = 0;
    gmatch.sfound = 0;
    gmatch.rfound = REG_NOMATCH;
    gmatch.depth = -1;
    if (cgmatch)
        gmatch.smatch = cgmatch->smatch;

    rc = _rregexec(string, &gmatch);
    /* Note: Number of matches found are truly the number found, but that
       arrays of:
       - regmatch_t     are 1 indexed
       - smatch_t       are 0 indexed
     */
    gmatch.pfound = gmatch.rfound - 1; /* Also last valid index */

    /* Update caller if requested using extended API. Restricted to relevant
     * data (status) */
    if (cgmatch) {
        cgmatch->sfound = gmatch.sfound;
        cgmatch->pfound = gmatch.pfound;
        cgmatch->depth = gmatch.depth;
    }
    free(rmatch);

    if (rc == REG_NOMATCH)
        pmatch[0] = (regmatch_t){ -1, -1 };
    return rc;
}

int gregexec(const regex_t *preg, const char *string, size_t nmatch,
             regmatch_t pmatch[], int eflags)
{
    return _gregexec(preg, string, nmatch, pmatch, eflags, NULL);
}

char *regsub(const char *r, const char *s, bool g, const char *t)
{
    UNUSED(r);
    UNUSED(s);
    UNUSED(g);
    UNUSED(t);
    return (char *)t;
}
