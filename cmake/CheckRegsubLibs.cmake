include(CheckLibraryExists)
include(CheckFunctionExists)

CHECK_FUNCTION_EXISTS(regcomp HAVE_POSIX_REGEX)

if (HAVE_POSIX_REGEX)
    option(REGSUB_USE_REGEX
        "Use POSIX regex" ON)
else ()
    message("** WARNING: No POSIX regexp available")
endif ()
