/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "sig.h"
#include "gensub_main.h"
#include "opts.h"
#include <liblog/log.h>
#include <liblog/assure.h>
#include <stdio.h>
#include <errno.h>
#include <gensub.h>
#include "config.h"
#include "common.h"

extern log_level liblog_filter_level; /* Short-cut to liblog filter variable */

/* Environment variable names */
#define ENV_LOGLEVEL "LOGLEVEL" /* Same as liblog default */

/* Syslog includes stderr or not */
#define INCLUDE_STDERR 1
#define NO_STDERR      0

struct opts opts = {
    /* clang-format off */
    .loglevel               = &liblog_filter_level,
    .daemon                 = 0,
    /* clang-format on */
};

/*
 * Comply to environment variables is set. Destination is opts struct. I.e.
 * it is populated in three passes where the last one has highest
 * precedence:
 *
 *  1  - Static initialization
 *  2  - Environment variable
 *  3  - Option
 * (4) - Same option later on command-line
 */
static void environment(void)
{
    char *str;

    str = getenv(ENV_LOGLEVEL);
    if (str != NULL) {
        int ll, ok = 1;
        if (!(ll = atoi(str)) && (str[0] != '0'))
            ll = liblog_str2loglevel(str, &ok);
        if (ok)
            liblog_set_verbosity(ll);
        else
            LOGE(ENV_LOGLEVEL ": Bad environment value: %s\n", str);
    }
}

int main(int argc, char **argv)
{
    int rc;
    int log_level = TEST_LOG_LEVEL;
    const char *pname = argv[0];
    int narg;
    opts.in = stdin;
    opts.out = stdout;
    char *outs;

    liblog_set_process_name("gensub");
    liblog_syslog_config(NO_STDERR);
    liblog_set_verbosity(log_level);
    LOGD("\"%s\" version v%s \n", pname, VERSION);

    environment();
    opts_init();
    ASSURE_E((rc = opts_parse(&argc, &argv, &opts)) >= 0, gensub_exit(254));
    LOGV("Parsed %d options.\n", rc);
    ASSURE_E(opts_check(&opts) == OPT_OK, gensub_exit(255));
    LOGV("Option passed rule-check OK\n", rc);
    siginit();

    narg = argc - 1;
    LOGV("%s (#args: %d)\n", pname, narg);
    for (int i = 1; i < argc; i++)
        LOGV("  %s\n", argv[i]);
    if (!(narg == 4 || narg == 3)) {
        fprintf(stderr, "Wrong number of arguments\n");
        opts_help(stderr, HELP_USAGE);
        return 1;
    }
    unsigned long long membeg = memavail();

    if (narg == 4)
        outs = gensub(argv[1], argv[2], argv[3][0], argv[4]);
    else
        outs = gensub(argv[1], argv[2], argv[3][0], NULL);

    if (outs) {
        puts(outs);
        free(outs);
    }

    unsigned long long memdiff;
    unsigned long long memend = memavail();
    if (memend >= membeg) {
        memdiff = memend - membeg;
        if (memdiff)
            LOGW("Memory difference (leak): %Lu\n", memdiff);
    } else {
        memdiff = membeg - memend;
        if (memdiff)
            LOGW("Memory difference (more): -%Lu\n", memdiff);
    }

    return rc;
}

void gensub_exit(int status)
{
    LOGV("gensub_exit initiated\n");

    free(opts.req_opts);
    exit(status);
}

void print_doc(void) __attribute__((weak, alias("__print_doc")));
void __print_doc(void)
{
    printf("TBD\n");
}
