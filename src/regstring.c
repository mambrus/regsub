/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* String-handling miscellaneous helper functions */

#include "common.h"
#include "regstring.h"
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Free a string-variable if not NULL before it gets assigned
 * again (prevent memory leakage)
 */
char **strassign(char **string)
{
    if (*string) {
        free(*string);
        *string = NULL;
    }
    return string;
}

/*
 * Free string if allocated and set NULL
 */
void strfree(char **string)
{
    if (*string) {
        free(*string);
        *string = NULL;
    }
}

/*
 *  Return a new string that is B appended to A. Free A before re-assigning
 *  and with the new string and return also a pointer to it.

 */
char *strnappnd(char **A, const char *B, size_t n)
{
    size_t sza, szb;
    char *string, *rp;
    UNUSED(rp);

    assert(A && *A);
    assert(B);
    if (!n)
        return *A;

    sza = strlen(*A);
    szb = strlen(B);
    szb = MIN(szb, n);

    string = calloc(1, sza + szb + 1);
    if (!string) {
        errno = ENOMEM;
        return NULL;
    }
    memcpy(string, *A, sza);
    rp = memcpy(&string[sza], B, szb);
    free(*A);
    *A = string;

    return string;
}

/*
 *  Return a new string that is B appended to A.
 */
char *strappnd(char **A, const char *B)
{
    int n;
    assert(B);
    n = strlen(B);

    return strnappnd(A, B, n);
}

/*
 *  Return a new string that is A prepended to B. Free B before re-assigning
 *  and with the new string and return also a pointer to it.
 */
char *strnprepend(const char *A, char **B, size_t n)
{
    size_t sza, szb;
    char *string, *rp;
    UNUSED(rp);

    assert(B && *B);
    assert(A);
    if (!n)
        return *B;

    szb = strlen(*B);
    sza = strlen(A);
    sza = MIN(sza, n);

    string = calloc(1, sza + 1 + szb);
    if (!string) {
        errno = ENOMEM;
        return NULL;
    }
    memcpy(string, A, sza);
    rp = memcpy(&string[sza], *B, szb);
    free(*B);
    *B = string;

    return string;
}

/*
 *  Return a new string that is A prepended to B. Free B before returning.
 */
char *strprepend(const char *A, char **B)
{
    int n;
    assert(A);
    n = strlen(A);

    return strnprepend(A, B, n);
}
