/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "regsub.h"
#include "regstring.h"
#include <stdio.h>
#include <regex.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <liblog/log.h>
#include <liblog/assure.h>
#include <assert.h>
#include "common.h"

#define BUF_SZ        1000
#define CHARS_MAX     (BUF_SZ - 1)
#define REGREF_REGEX  "\\\\[0-9]|&"
#define REGREF_CFLAGS (REG_EXTENDED | REG_NEWLINE)

/*
 *  Dumps a regmatch_t content
 */
void rm_dump(const char *name, regmatch_t rm[])
{
    char os[BUF_SZ] = { 0 };
    int j = 0, i = 0;

    j += sprintf(os, "%7s :", name);
    while (rm[i].rm_so >= 0) {
        j += sprintf(&os[j], "{%2d,%2d}", rm[i].rm_so, rm[i].rm_eo);
        i++;
        if (rm[i].rm_so >= 0) {
            j += sprintf(&os[j], ", ");
        }
    }
    LOGD("%s", os);
}

static regex_t refreg;
static bool is_compiled = false;
/*
 *  Get the "refreg" variable reference, compile it if necessary,
 *
 *  "refreg" is a regular expression for back-references in substitution
 *  strings. I.e. \0, \1..\9, &
 */
regex_t *get_refreg(void)
{
    int rc;
    UNUSED(rc);

    if (!(is_compiled)) {
        rc = REG(regcomp, &refreg, REGREF_REGEX, REGREF_CFLAGS);
        assert(rc == 0); /* Should never fail */
        is_compiled = true;
    }
    return &refreg;
}

/*
 * Substitute matches in Target-string (t) using match-list (tm) with
 * expanded Substitutions originating from string (s), in turn using
 * match-list (sm) with lambda-function (getstr).
 */
char *sub(const char *s, const char *t, regmatch_t tm[], regmatch_t sm[],
          getstr_t getstr)
{
    int nr = MATCHES(tm, MAX_MATCH);
    int ci = 0, tdiff, i, rm_eo, rm_diff;
    char *string =
        strdup(""); /* Start with empty string required by strappend() */

    /* Pre-calculate the remaining chunk. This is done beforhand as tm[] can
     * change */
    rm_eo = tm[nr - 1].rm_eo;
    rm_diff = strlen(t) - rm_eo;
    assert(rm_eo >= 0);
    assert(rm_diff >= 0);

    for (ci = 0, i = 1; i < nr; i++) {
        /* If there is a gap before substitution, cover gap with same part from
         * original. */
        if (i != 1) {
            tdiff = (tm[i].rm_so - tm[i - 1].rm_eo);
            tdiff = !(tdiff < 0) ? tdiff : 0;
            if (tdiff) {
                ci = tm[i - 1].rm_eo;
                strnappnd(&string, &t[ci], tdiff);
            }
        } else {
            /* Note: Concider, [0] not [1] in case repetitive wo changes */
            if (tm[0].rm_so) {
                strnappnd(&string, t, tm[0].rm_so);
            }
        }
        /* Substitute */
        string = strappnd(&string, getstr(s, t, tm, sm, i));
    }

    /* Close the remaining chunk unless it was already covered by a match */
    if (rm_diff > 0)
        strnappnd(&string, &t[rm_eo], rm_diff);

    return string;
}
