#
# Tools necessary for build
#
find_program(EXE_HSUBST
    hsubst
)
if(EXE_HSUBST STREQUAL "EXE_HSUBST-NOTFOUND")
    message(FATAL_ERROR "Please install hsubst")
endif()
find_program(EXE_HELP2MAN
    help2man
)
if(EXE_HELP2MAN STREQUAL "EXE_HELP2MAN-NOTFOUND")
    message(FATAL_ERROR "Please install help2man")
endif()
find_program(EXE_GROFF
    groff
)
if(EXE_GROFF STREQUAL "EXE_GROFF-NOTFOUND")
    message(FATAL_ERROR "Please install groff")
endif()
find_program(EXE_DOXYGEN
    doxygen
)
if(EXE_DOXYGEN STREQUAL "EXE_DOXYGEN-NOTFOUND")
    message(FATAL_ERROR "Please install doxygen")
endif()
find_program(EXE_DOXY2MAN
    doxy2man
)
if(EXE_DOXY2MAN STREQUAL "EXE_DOXY2MAN-NOTFOUND")
    message(FATAL_ERROR "Please install doxy2man (https://github.com/gsauthof/doxy2man also see README.md)")
endif()


#
# Local tools
#
find_program(EXE_SIGGEN
    siggen
    PATHS ${CMAKE_CURRENT_SOURCE_DIR}/bin
    PATHS ${CMAKE_SOURCE_DIR}/bin
)
if(EXE_SIGGEN STREQUAL "EXE_SIGGEN-NOTFOUND")
    message(FATAL_ERROR "Please install siggen")
endif()
find_program(EXE_DOCGEN
    docgen
    PATHS ${CMAKE_CURRENT_SOURCE_DIR}/bin
    PATHS ${CMAKE_SOURCE_DIR}/bin
)
if(EXE_DOCGEN STREQUAL "EXE_DOCGEN-NOTFOUND")
    message(FATAL_ERROR "Please install docgen (map-page -> c-file)")
endif()
find_program(EXE_GEN_DOXY2MAN
    gen-doxy2man
    PATHS ${CMAKE_CURRENT_SOURCE_DIR}/bin
    PATHS ${CMAKE_SOURCE_DIR}/bin
)
if(EXE_GEN_DOXY2MAN STREQUAL "EXE_GEN_DOXY2MAN-NOTFOUND")
    message(FATAL_ERROR "Please install gen-doxy2man")
endif()
