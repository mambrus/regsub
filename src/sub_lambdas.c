/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
 * Client-functions of sub() and corresponding lamda-functions implementing
 * features togeather
 ***************************************************************************/
#include "regsub.h"
#include "regstring.h"
#include <stdio.h>
#include <regex.h>
#include <stdlib.h>
#include <liblog/log.h>
#include <liblog/assure.h>
#include <assert.h>
#include "common.h"

#ifdef NOTNOW
static const char *file_name = __FILE_NAME__; /* Expand literal string only
                                                   once */
#endif

/***************************************************************************
 * Lamdas
 ***************************************************************************/
char *lam_backref_str(const char *s, const char *t, regmatch_t tm[],
                      regmatch_t sm[], int k)
{
    size_t zt, zs;
    int tdiff = 0;

    assert(tm);
    assert(sm);
    zt = MATCHES(tm, MAX_MATCH);
    zs = MATCHES(sm, MAX_MATCH);
    LOGV("s: \"%s\" t: \"%s\"\n", s, t);
    LOGV("size(t)=%d, size(s)=%d, k: %d\n", zt, zs, k);
    RM_DUMP(tm);
    RM_DUMP(sm);

    char *string = NULL;
    if ((unsigned)k < zt) {
        size_t c = t[tm[k].rm_so + 1];
        size_t a = t[tm[k].rm_so];
        LOGD("c(%d,%c), a(%d,%c)", c, c, a, a);
        if (c > '0' && c < '9') {
            c = c - '0';
            if (c < zs + 1) {
                tdiff = sm[c].rm_eo - sm[c].rm_so;
                STRASGN(string) = strndup(&s[sm[c].rm_so], tdiff);
            } else {
                LOGW(
                    "Numeric reference for k=%d ignored, out of range: %d (%c)",
                    k, c, c);
            }
        } else if (c == '0' || (a == '&')) {
            tdiff = sm[0].rm_eo - sm[0].rm_so;
            strnappnd(&string, &s[sm[0].rm_so], tdiff);
        } else {
            LOGE("Rerence error detected for k=%d: %d (%c)", k, c, c);
        }
        LOGV("c o: %c \"%s\"\n", (c <= 9) ? c + '0' : c, string);
    }
    if (!string) {
        LOGW("%s found no string to return. Returning empty but valid string\n",
             __FUNCTION__);
        string = strdup("");
    }
    return string;
}

/***************************************************************************
 * Outer API's
 ***************************************************************************/
/*
 * For the substituion string s, exand it with back-references
 */
char *sub_backref_str(SUBST_SIGNATURE)
{
    return sub(s, t, sm, tm, lam_backref_str);
}

/*
 * Substitute matches in t with strings
 */
char *sub_replace_str(SUBST_SIGNATURE)
{
    char *replace = NULL;

    /***********************************************************************
    * Lamda
    ************************************************************************/
    char *get_replace_str(SUBST_SIGNATURE, int k)
    {
        UNUSED_ALL_SUB;
        UNUSED(k);
        /* This will always evaluate to the same string. No need to re-run */
        if (replace)
            return replace;
        return NULL;
    }

    /***********************************************************************
    * Main call
    ************************************************************************/
    return sub(s, t, tm, sm, get_replace_str);
}
