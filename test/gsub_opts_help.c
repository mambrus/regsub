/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gsub_main.h"
#include "opts.h"
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <getopt.h>
#include "config.h"
#include "common.h"

const char *program_version = "gsub " VERSION;

void opts_help(FILE *file, int flags)
{
    if (file && flags & HELP_USAGE) {
        fprintf(file, "%s",
                "Usage: gsub [OPTION...] r s t\n"
                "            [-i FILENAME] [--infile=FILENAME] \n"
                "            [-o FILENAME] [--out=FILENAME] \n"
                "            [-l level] [--loglevel=level] \n"
                "            [--documentation]\n"
                "            [--help] [--usage] [--version]\n");
        fflush(file);
    }
    if (file && flags & HELP_LONG) {
        fprintf(
            file, "%s",
            "Usage: gsub [OPTION...] r s t\n"
            "\n"
            "gensub (project: " PROJ_NAME ", version:" VERSION ") test API\n"
            "\n"
            "#include <gensub.h>\n\n"
            "int gsub(const char *r, const char *s, char *t[]);\n"
            "\n \n"
            "Generic options:\n"
            "  -i, --infile=NAME          Read from FILE\n"
            "  -o, --outfile=NAME         Write to FILE\n"
            "  -l, --loglevel=level       Set the loglevel level.\n"
            "                             Levels, listed in increasing loglevel, are:\n"
            "                             critical, error, warning, info, debug, verbose\n"
            "Special:\n"
            "  -D, --documentation        Output full documentation, then exit\n"
            "  -z, --daemon               Run as a daemon\n"
            "  -h, --help                 Print this help\n"
            "  -u, --usage                Give a short usage message\n"
            "  -v, --version              Print program version\n"
            "\n"
            "Mandatory or optional arguments to long options are also mandatory or optional\n"
            "for any corresponding short options.\n"
            "\n"
            "Read the manual using 'man gsub' or by passing option -D\n"
            "\n"
            "Report bugs to <michael@ambrus.se>\n");
        fflush(file);
    }

    if (file && flags & HELP_VERSION) {
        fprintf(file, "%s\n", program_version);
        fflush(file);
    }

    if (file && flags & HELP_TRY) {
        fprintf(file, "%s",
                "Try `gsub --help' or `gsub --usage' for more information.\n");
        fflush(file);
    }

    if (file && flags & HELP_EXIT)
        gsub_exit(0);

    if (file && flags & HELP_EXIT_ERR)
        gsub_exit(1);
}
