/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "regsub.h"
#include "regstring.h"
#include <stdio.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <stdint.h>
#include <errno.h>
#include <liblog/log.h>
#include <liblog/assure.h>
#include <assert.h>
#include "common.h"

static const char *file_name = __FILE_NAME__; /* Expand literal string only
                                                   once */

char *gensub(const char *r, const char *s, char h, const char *_t)
{
    regmatch_t pmatch[MAX_MATCH + 1] = { [0 ... MAX_MATCH] = { -2, -2 } };
    regmatch_t rmatch[MAX_MATCH + 1] = { [0 ... MAX_MATCH] = { -2, -2 } };
    smatch_t smatch[MAX_MATCH + 1] = { 0 };

    struct gmatch gmatch = { .nmatch = MAX_MATCH + 1,
                             .pmatch = pmatch,
                             .rmatch = rmatch,
                             .smatch = smatch,
                             .pfound = 0,
                             .rfound = 0,
                             .sfound = 0,
                             .depth = 0,
                             .cflags = DEF_CFLAGS,
                             .eflags = DEF_EFLAGS,
                             .rglobal = (h == 'G' | h == 'g') ? true : false,
                             .rindex = (h == 'G' | h == 'g') ?
                                           0xff :
                                           (h > 9 ? h - '0' : h) };
    bool stdio = false;
    char *string = NULL;
    char *s_replace = NULL;
    int rc;
    char *t = (char *)_t;

    if (!(r && s)) {
        LOGE("%: Argument r or s NULL detected: %p%p\n", file_name, r, s);
        errno = EINVAL;
        return NULL;
    }

    if ((gmatch.rindex < 1) || (gmatch.rindex > 9)) {
        if (!((h == 'G') || (h == 'g'))) {
            LOGW("gensub: third argument `%c' treated as 1\n", h);
            gmatch.rindex = 1;
        }
    }
    if (r[0] != '^')
        gmatch.cflags |= REG_NEWLINE;

    /* Compile the regexp */
    if (REG(regcomp, &gmatch.regex, r, gmatch.cflags))
        return NULL;

    /* Read and allocate from stdin (mandated by API) */
    if (!t) {
        stdio = true;
        int rc = scanf("%m[^\n]", &t); /*Read arbitrary length line (%m =
                                           allocated) */
        switch (rc) {
            case 0:
                LOGW("Nothing read from stdin\n");
                break;
            case EOF:
                LOGW("EOF read from stdin\n");
                break;
            default:
                (void)(0);
        }
    }
    /* Re-matches (pmatch) and sub-match (smatch) groups for target-string t
     */
    rc = REG(_gregexec, &gmatch.regex, t, gmatch.nmatch, gmatch.pmatch,
             gmatch.eflags, &gmatch);

    if (rc) {
        /* No matches. Just verbatim copy target */
        STRASGN(string) = strdup(t);
        goto fini;
    }

    /*  Expand back-references to substitute with for current (sub-)match if
     *  possible. If no back-references, fall back to return (s).
     *  Determining inability to access back-references is done only once.
     *  I.e. once inability is determined, it's not done again until main
     *  function is re-run */
    bool can_expand = true;
    bool has_sm = false;
    bool has_tried_sm = false;
    char *expand_replace(const char *s,   /* Replacement string OR formatted
                                           back-reference (\1 etc) */
                         const char *t,   /* Target string */
                         regmatch_t tm[], /* Matches in target-string. Used
                                               here only when a match is
                                               ignored (see s) */
                         regmatch_t sm[], /* Substitution string
                                               back-reference matches (see s)
                                             */
                         int k            /* Current match processed
                                   (ascending order required */
    )
    {
        int rc = -1;
        if (!can_expand) {
            if (!s_replace)
                s_replace = strdup(s);
            return s_replace;
        }
        /* Re-matches for references in substitution-string s */
        if (!has_sm) {
            if (!has_tried_sm) {
                rc = REG(gregexec, get_refreg(), s, gmatch.nmatch, sm,
                         gmatch.eflags);
                has_tried_sm = true;
            }
            if (rc == 0)
                has_sm = true;
        }
        if (has_sm) {
            regmatch_t om[MAX_MATCH + 1];
            rc = REG(regexec, &gmatch.regex, &t[tm[k].rm_so], gmatch.nmatch, om,
                     gmatch.eflags);
            if (rc == 0)
                STRASGN(s_replace) =
                    sub(&t[tm[k].rm_so], s, sm, om, lam_backref_str);
            else
                STRASGN(s_replace) = sub(t, s, sm, tm, lam_backref_str);
        } else {
            can_expand = false;
            STRASGN(s_replace) = strdup(s);
        }
        return s_replace;
    }

    /* Get replacement string with back-references expanded from (t) if
     * necessary, using the same reg pattern (but always global) */
    bool ingroup = false;
    int lgi = 0; /* Pending group index. */
    char *get_replace_str(const char *s, const char *t, regmatch_t tm[],
                          regmatch_t sm[], int k)
    {
        char *rets = NULL;
        int ci, tdiff;

        if (ingroup) {
            /* Check if we are still ingroup */
            if (k > gmatch.smatch[lgi].end) {
                lgi++; /* Not in last group anymore */
                ingroup = false;
            }
        }

        /* This will always evaluate to the same string. No need to re-run */
        if (gmatch.rglobal || (ingroup && (1 == gmatch.rindex))) {
            /* This will always evaluate to the same string. Should be
             * deducted already */
            if (!ingroup)
                STRASGN(rets) = expand_replace(s, t, tm, sm, k);
            else
                STRASGN(rets) = strdup("");
            goto done;
        }
        if (k == gmatch.rindex) {
            if (ingroup && (1 != gmatch.rindex))
                goto igno_subst;

            STRASGN(rets) = expand_replace(s, t, tm, sm, k);
            goto done;
        }

igno_subst:
        /* Replace with a strndup() copy of what was originally there. I.e.
         * don't substitute content, but still a new string is required for
         * the algo in sub() */
        ci = tm[k].rm_so;
        tdiff = tm[k].rm_eo - ci;
        STRASGN(rets) = strndup(&t[ci], tdiff);
done:
        /* Deduct if what was just was processed was within a group or not
         * and if to store this state into next iteration (i.e. group is not
         * finished)
         */
        if (!ingroup) {
            /* Check if entered "inGroup" state */
            if (k == gmatch.smatch[lgi].begin) {
                ingroup = true;
            }
        }
        return rets;
    }

    STRASGN(string) = sub(s, t, gmatch.pmatch, gmatch.rmatch, get_replace_str);

fini:
    if (stdio) {
        free(t);
    }

    if (s_replace)
        free(s_replace);

    regfree(&gmatch.regex);
    return string;
}

int gsub(const char *r, const char *s, char *t[])
{
    UNUSED(r);
    UNUSED(s);
    UNUSED(t);
    return 0;
}
